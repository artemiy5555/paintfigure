public class RovnobedrBot {
    public static void main(String[] args){
        rovnobedrBot(5);
    }
    public static void rovnobedrBot(int rectangle) {
        for (int i = rectangle; i >= 0; i--){

            for (int j = rectangle; j > i; j--){
                System.out.print(" ");
            }

            for (int j1 = 0; j1 <= i; j1++){
                System.out.print("*");
            }

            for (int j = 0; j < i; j++){
                System.out.print("*");
            }

            for (int j = rectangle; j > i; j--){
                System.out.print(" ");
            }

            System.out.println();
        }
    }
}
